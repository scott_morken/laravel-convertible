<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/6/17
 * Time: 8:22 AM
 */

namespace Smorken\Convertible\Models\Traits;

use Illuminate\Database\Eloquent\Relations\Relation;
use LogicException;
use Smorken\Convertible\Models\Relations\FakeBelongsTo;
use Smorken\Convertible\Models\Relations\FakeHasMany;

trait FakeRelations
{

    protected $relations = [];

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @param  string $related
     * @param array $attributes
     * @param  string $relation
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function fakeBelongsTo($related, array $attributes = [], $relation = null)
    {
        // If no relation name was given, we will use this debug backtrace to extract
        // the calling method's name and use that as the relationship name as most
        // of the time this will be what we desire to use for the relationships.
        if (is_null($relation)) {
            list($current, $caller) = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);

            $relation = $caller['function'];
        }

        $instance = is_string($related) ? new $related : $related->newInstance();

        return new FakeBelongsTo($instance, $this, $attributes, $relation);
    }

    /**
     * Define a one-to-many relationship
     *
     * @param  string $related
     * @param array $attributes
     * @param  string $relation
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function fakeHasMany($related, array $attributes = [], $relation = null)
    {
        // If no relation name was given, we will use this debug backtrace to extract
        // the calling method's name and use that as the relationship name as most
        // of the time this will be what we desire to use for the relationships.
        if (is_null($relation)) {
            list($current, $caller) = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);

            $relation = $caller['function'];
        }

        $instance = is_string($related) ? new $related : $related->newInstance();

        return new FakeHasMany($instance, $this, $attributes, $relation);
    }

    /**
     * Get a relationship.
     *
     * @param  string  $key
     * @return mixed
     */
    public function getRelationValue($key)
    {
        // If the key already exists in the relationships array, it just means the
        // relationship has already been loaded, so we'll just return it out of
        // here because there is no need to query within the relations twice.
        if ($this->relationLoaded($key)) {
            return $this->relations[$key];
        }

        // If the "attribute" exists as a method on the model, we will just assume
        // it is a relationship and will load and return results from the query
        // and hydrate the relationship's value on the "relationships" array.
        if (method_exists($this, $key)) {
            return $this->getRelationshipFromMethod($key);
        }
    }

    /**
     * Get a relationship value from a method.
     *
     * @param  string  $method
     * @return mixed
     *
     * @throws \LogicException
     */
    protected function getRelationshipFromMethod($method)
    {
        $relations = $this->$method();

        if (! $relations instanceof Relation) {
            throw new LogicException('Relationship method must return an object of type '
                                     .'Illuminate\Database\Eloquent\Relations\Relation');
        }

        return $this->relations[$method] = $relations->getResults();
    }

    /**
     * Get all the loaded relations for the instance.
     *
     * @return array
     */
    public function getRelations()
    {
        return $this->relations;
    }

    /**
     * Get a specified relationship.
     *
     * @param  string  $relation
     * @return mixed
     */
    public function getRelation($relation)
    {
        return $this->relations[$relation];
    }

    /**
     * Determine if the given relation is loaded.
     *
     * @param  string  $key
     * @return bool
     */
    public function relationLoaded($key)
    {
        return array_key_exists($key, $this->relations);
    }

    /**
     * Set the specific relationship in the model.
     *
     * @param  string  $relation
     * @param  mixed  $value
     * @return $this
     */
    public function setRelation($relation, $value)
    {
        $this->relations[$relation] = $value;

        return $this;
    }

    /**
     * Set the entire relations array on the model.
     *
     * @param  array  $relations
     * @return $this
     */
    public function setRelations(array $relations)
    {
        $this->relations = $relations;

        return $this;
    }
}
