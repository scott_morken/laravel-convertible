<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/28/16
 * Time: 3:01 PM
 */

namespace Smorken\Convertible\Models\Traits;

use Smorken\Convertible\Contracts\VO;

trait Convert
{

    /**
     * @var VO
     */
    protected $vo;

    /**
     * @param null $conversion
     * @return VO
     */
    public function toVo($conversion = null)
    {
        return $this->getVo()->newInstance($this->convert($conversion));
    }

    /**
     * @return VO
     */
    public function getVo()
    {
        if (!$this->vo) {
            $this->vo = new \Smorken\Convertible\Models\VO();
        }
        return $this->vo;
    }

    public function setVo(VO $vo)
    {
        $this->vo = $vo;
    }

    /**
     * @param null $conversion
     * @return mixed
     */
    public function convert($conversion = null)
    {
        $converted = [];
        if ($conversion === null) {
            $conversion = $this->getConversion();
        }
        foreach ($conversion as $vo_attr => $converter) {
            $converted[$vo_attr] = $this->convertAttribute($vo_attr, $converter);
        }
        return $converted;
    }

    public function getConversion($key = 'default')
    {
        $conversions = $this->conversions();
        if (isset($conversions[$key])) {
            return $conversions[$key];
        }
        return [];
    }

    protected function convertAttribute($attribute_name, $converter)
    {
        if ($converter instanceof \Closure) {
            return $converter($attribute_name, $this);
        } elseif (is_string($converter) &&
            (array_key_exists($converter, $this->attributes) || $this->hasGetMutator($converter))
        ) {
            return $this->getAttributeValue($converter);
        }
        return $converter;
    }

    public function conversions()
    {
        return [
            'default' => [],
        ];
    }

    public function __get($key)
    {
        return $this->getAttribute($key);
    }

    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);
        if (is_null($value)) {
            $value = $this->getAttributeConverted($key);
        }
        return $value;
    }

    /**
     * @param string $attribute_name
     * @param string $key
     * @return mixed
     */
    public function getAttributeConverted($attribute_name, $key = 'default')
    {
        $c = $this->getNameFromConversion($attribute_name, $key);
        return $this->convertAttribute($attribute_name, $c);
    }

    protected function getNameFromConversion($attribute_name, $key = 'default')
    {
        $c = null;
        $conversion = $this->getConversion($key);
        $c = $this->fromConversion($conversion, $attribute_name);
        return $this->checkCase(($c ?: $attribute_name));
    }

    protected function checkCase($attr)
    {
        if (!$attr instanceof \Closure) {
            $lower = strtolower($attr);
            if (!array_key_exists($attr, $this->attributes) && array_key_exists($lower, $this->attributes)) {
                $attr = $lower;
            }
        }
        return $attr;
    }

    protected function fromConversion($conversion, $attribute_name)
    {
        $c = null;
        if (isset($conversion[$attribute_name])) {
            $c = $conversion[$attribute_name];
        }
        $attribute_name = strtolower($attribute_name);
        if (is_null($c) && isset($conversion[$attribute_name])) {
            $c = $conversion[$attribute_name];
        }
        return $c;
    }
}
