<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 2/5/16
 * Time: 7:09 AM
 */

namespace Smorken\Convertible\Models\Eloquent;

use Carbon\Carbon;
use Smorken\Convertible\Contracts\Convertible;
use Smorken\Convertible\Models\ReadOnlyException;
use Smorken\Convertible\Models\Traits\Convert;
use Smorken\Ext\Database\Eloquent\Model;

abstract class BaseModel extends Model implements Convertible
{

    use Convert;

    protected $columns = [];

    protected $relation_map = [];

    public $timestamps = false;

    public $incrementing = false;

    protected static $readonly = true;

    public static function allowWrite()
    {
        self::$readonly = false;
    }

    public static function disallowWrite()
    {
        self::$readonly = true;
    }

    public function createOrUpdate($keys, array $data)
    {
        $this->throwReadOnlyException();
        return parent::createOrUpdate($keys, $data);
    }

    public function save(array $options = [])
    {
        $this->throwReadOnlyException();
        return parent::save($options);
    }

    protected function throwReadOnlyException()
    {
        if (self::$readonly) {
            throw new ReadOnlyException(sprintf('%s is read only.', $this->getTable()));
        }
    }

    public function scopeDefaultColumns($query, $alias = null)
    {
        if ($this->columns) {
            $query->select($this->getColumns(true, $alias));
        }
        return $query;
    }

    protected function simpleWhere($query, $key, $value)
    {
        if ($value !== null) {
            $query->where($key, '=', $value);
        }
        return $query;
    }

    public function getColumns($qualified = false, $alias = null)
    {
        if ($qualified || $alias !== null) {
            return $this->qualifyColumns($this->columns, $alias);
        }
        return $this->columns;
    }

    protected function qualifyColumns($columns, $alias = null)
    {
        $self = $this;
        return array_map(
            function ($v) use ($self, $alias) {
                return sprintf('%s.%s', $alias ?: $self->getTable(), $v);
            },
            $columns
        );
    }

    protected function whereTermLike($column, $query, $term_id, $alias = null)
    {
        if ($term_id === null) {
            return $query;
        }
        if (strlen($term_id) === 3) {
            $term_id = '_' . $term_id;
            $query->where(
                sprintf('%s.%s', $alias ?: $this->getTable(), $column),
                'LIKE',
                $term_id
            );
        } else {
            $query->where(sprintf('%s.%s', $alias ?: $this->getTable(), $column), '=', $term_id);
        }
        return $query;
    }

    protected function toCarbonDate($column)
    {
        $value = $this->arrayGet($this->attributes, $column, null);
        if ($value) {
            return Carbon::parse($value);
        }
        return null;
    }

    protected function getFromArray($options, $key)
    {
        return $this->arrayGet($options, $key, $key);
    }

    protected function getRelationClass($relation_name)
    {
        $r = $this->arrayGet($this->relation_map, $relation_name);
        if ($r === null) {
            throw new \InvalidArgumentException("$relation_name is not a valid relation.");
        }
        return $r;
    }

    public function setRelationMap(array $map)
    {
        $this->relation_map = $map;
    }

    public function getRelationMap()
    {
        return $this->relation_map;
    }

    protected function arrayGet($arr, $key, $default = null)
    {
        if (isset($arr[$key])) {
            return $arr[$key];
        }
        $key = strtolower($key);
        if (isset($arr[$key])) {
            return $arr[$key];
        }
        return $default;
    }
}
