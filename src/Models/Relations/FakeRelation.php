<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/6/17
 * Time: 7:51 AM
 */

namespace Smorken\Convertible\Models\Relations;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class FakeRelation extends \Illuminate\Database\Eloquent\Relations\Relation
{

    protected $related;

    protected $parent;

    protected $relation;

    public function __construct($related, $parent, $relation = null)
    {
        $this->related = $related;
        $this->parent = $parent;
        $this->relation = $relation;
        $this->addConstraints();
    }

    /**
     * Set the base constraints on the relation query.
     *
     * @return void
     */
    public function addConstraints()
    {
        $this->initRelation([$this->parent], $this->relation);
    }

    /**
     * Set the constraints for an eager load of the relation.
     *
     * @param  array $models
     * @return void
     */
    public function addEagerConstraints(array $models)
    {
        // do nothing
    }

    /**
     * Initialize the relation on a set of models.
     *
     * @param  array $models
     * @param  string $relation
     * @return array
     */
    public function initRelation(array $models, $relation)
    {
        foreach ($models as $model) {
            $this->handleRelation($model, $relation); //load the actual relation data into the model
        }
        return $models;
    }

    protected function handleSetRelation($model, $related, $relation)
    {
        if (method_exists($model, 'setRelation')) {
            $model->setRelation($relation, $related);
        } else {
            $model->$relation = $related;
        }
    }

    abstract protected function handleRelation($model, $relation);

    /**
     * Match the eagerly loaded results to their parents.
     *
     * @param  array $models
     * @param  \Illuminate\Database\Eloquent\Collection $results
     * @param  string $relation
     * @return array
     */
    public function match(array $models, Collection $results, $relation)
    {
        return $models; //relations should already be loaded at this point
    }

    public function getEager()
    {
        return new Collection();
    }

    /**
     * Get the results of the relationship.
     *
     * @return mixed
     */
    public function getResults()
    {
        if (method_exists($this->parent, 'getRelation')) {
            return $this->parent->getRelation($this->relation);
        }
        return $this->parent->{$this->relation};
    }

    protected function createModelAsRelation($model)
    {
        $model = $this->getModelFromModel($model);
        $attributes = $this->buildAttributeListFromModel($model, $this->getAttributeConversionList());
        $related_model = $this->related;
        if ($related_model instanceof Model) {
            $m = $this->createEloquentModel($related_model, $attributes);
        } else {
            $m = $this->createGenericModel($related_model, $attributes);
        }
        return $m;
    }

    protected function createEloquentModel(Model $model, $attributes)
    {
        $m = $model->newInstance([], true);
        $m->forceFill($attributes);
        return $m;
    }

    protected function createGenericModel($model, $attributes)
    {
        $m = new $model;
        if (method_exists($m, 'setAttributes')) {
            $m->setAttributes($attributes);
        } else {
            foreach ($attributes as $k => $v) {
                $m->$k = $v;
            }
        }
        return $m;
    }

    protected function buildAttributeListFromModel($model, $attributes)
    {
        $built = [];
        if (empty($attributes)) {
            $built = $this->getAttributesAsArrayFromModel($model);
        } else {
            foreach ($attributes as $local => $parent) {
                $val = $model->$parent;
                if (is_int($local)) {
                    $built[$parent] = $val;
                } else {
                    $built[$local] = $val;
                }
            }
        }
        return $built;
    }

    protected function getModelFromModel($model)
    {
        $attrs = $this->getAttributesAsArrayFromModel($model);
        if (array_key_exists($this->relation, $attrs)) {
            return array_get($attrs, $this->relation);
        }
        return $model;
    }

    protected function getAttributesAsArrayFromModel($model)
    {
        $attributes = [];
        if (method_exists($model, 'getAttributes')) {
            $attributes = $model->getAttributes();
        } elseif (method_exists($model, 'toArray')) {
            $attributes = $model->toArray();
        } elseif (is_array($model)) {
            $attributes = $model;
        }
        return $attributes;
    }

    protected function getAttributeConversionList()
    {
        if (property_exists($this, 'attributes')) {
            return $this->attributes;
        }
        return [];
    }
}
