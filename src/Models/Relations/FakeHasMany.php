<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/6/17
 * Time: 8:18 AM
 */

namespace Smorken\Convertible\Models\Relations;

use Illuminate\Support\Collection;

class FakeHasMany extends FakeBelongsToOrHasMany
{

    protected function handleRelation($model, $relation)
    {
        $related = $this->getRelatedCollection($model, $relation);
        $this->handleSetRelation($model, $related, $relation);
    }

    protected function getRelatedCollection($model, $relation)
    {
        $collection = new Collection();
        if (is_array($model)) {
            $items = array_get($model, $relation, []);
        } else {
            $items = $model->$relation;
            if (!$items) {
                $items = [];
            }
        }
        foreach ($items as $item) {
            $collection->push($this->createModelAsRelation($item));
        }
        return $collection;
    }
}
