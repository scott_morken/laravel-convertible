<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/6/17
 * Time: 8:03 AM
 */

namespace Smorken\Convertible\Models\Relations;

abstract class FakeBelongsToOrHasMany extends FakeRelation
{

    protected $attributes = [];

    public function __construct($related, $parent, array $attributes = [], $relation = null)
    {
        $this->attributes = $attributes;
        parent::__construct($related, $parent, $relation);
    }

    protected function handleRelation($model, $relation)
    {
        $related = $this->createModelAsRelation($model);
        $this->handleSetRelation($model, $related, $relation);
    }
}
