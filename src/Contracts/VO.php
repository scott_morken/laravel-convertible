<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/29/16
 * Time: 7:07 AM
 */

namespace Smorken\Convertible\Contracts;

/**
 * Interface VO
 * @package Smorken\Convertible\Contracts
 */
interface VO
{

    /**
     * @param array $attributes
     * @return VO
     */
    public function newInstance(array $attributes = []);

    /**
     * @param $key
     * @return mixed
     */
    public function getAttribute($key);

    /**
     * @param $key
     * @param $value
     * @return void
     */
    public function setAttribute($key, $value);

    /**
     * @param $key
     * @return bool
     */
    public function has($key);

    /**
     * Mass set $this->attributes
     * @param array $attributes
     */
    public function setAttributes(array $attributes);

    /**
     * @return array
     */
    public function getAttributes();
}
