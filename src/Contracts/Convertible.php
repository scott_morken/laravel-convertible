<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/28/16
 * Time: 10:04 AM
 */

namespace Smorken\Convertible\Contracts;

/**
 * Interface Convertible
 * @package Smorken\Convertible\Contracts
 *
 * Converts a model to a common value object
 */
interface Convertible
{

    /**
     * @param null $conversion
     * @return mixed
     */
    public function convert($conversion = null);

    /**
     * Returns an array of conversion keys with an array of
     * 'attribute_name' => 'model_name' || \Closure
     * @return array
     */
    public function conversions();

    /**
     * @param string $key
     * @return mixed
     */
    public function getAttribute($key);

    /**
     * @param string $attribute_name
     * @return mixed
     */
    public function getAttributeConverted($attribute_name);

    /**
     * @param null $conversion
     * @return VO
     */
    public function toVo($conversion = null);

    /**
     * @return VO
     */
    public function getVo();

    /**
     * @param VO $vo
     * @return mixed
     */
    public function setVo(VO $vo);
}
