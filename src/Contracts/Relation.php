<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/6/17
 * Time: 8:32 AM
 */

namespace Smorken\Convertible\Contracts;

interface Relation
{

    /**
     * Get a relationship.
     *
     * @param  string $key
     * @return mixed
     */
    public function getRelationValue($key);

    /**
     * Get all the loaded relations for the instance.
     *
     * @return array
     */
    public function getRelations();

    /**
     * Get a specified relationship.
     *
     * @param  string $relation
     * @return mixed
     */
    public function getRelation($relation);

    /**
     * Determine if the given relation is loaded.
     *
     * @param  string $key
     * @return bool
     */
    public function relationLoaded($key);

    /**
     * Set the specific relationship in the model.
     *
     * @param  string $relation
     * @param  mixed $value
     * @return $this
     */
    public function setRelation($relation, $value);

    /**
     * Set the entire relations array on the model.
     *
     * @param  array $relations
     * @return $this
     */
    public function setRelations(array $relations);
}
