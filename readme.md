## Convertible Model extension for Laravel 5+

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Installation

Extend the necessary models as needed

```
class MyModel extends BaseModel
{
  protected $columns = [
    'foo',
    'bar',
    'biz',
    'baz',
  ];
  
  protected $relation_map = [
    'foos' => '\Namespace\FooModel',
  ];
  
  public function conversions()
  {
    return [
      'default' => [
        'my_foo' => 'foo',
        'my_bar' => 'bar',
        'my_biz' => 'biz',
        'my_baz' => function($attr, $self) {
          return $self->getMyBazDesc();
        },
      ],
    ];
  }
  
  public function getMyBazDesc()
  {
    return 'bizbazbuz';
  }
  
  public function foos()
  {
    $rel = $this->getRelationClass(__FUNCTION__);
    return $this->hasMany($rel);
  }
  
}
```
