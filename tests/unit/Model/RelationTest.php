<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 6/6/17
 * Time: 8:38 AM
 */

use Smorken\Convertible\Models\Traits\FakeRelations;
use Smorken\Convertible\Models\VO;

class RelationTest extends \PHPUnit\Framework\TestCase
{

    public function testBelongsToCreatesModelAsNestedArrayOnParent()
    {
        $m = new RelationStub1(['id' => 'rs1', 'descr' => 'RS 1', 'rs2' => ['id' => 'rs2', 'descr' => 'RS 2']]);
        $rs2 = $m->rs2;
        $this->assertInstanceOf(RelationStub2::class, $rs2);
        $this->assertEquals('rs2', $rs2->id);
    }

    public function testBelongsToCreatesModelAsAttributesOnParent()
    {
        $m = new RelationStub1(['id' => 'rs1', 'descr' => 'RS 1', 'rs2_id' => 'rs2', 'rs2_descr' => 'RS 2']);
        $rs2 = $m->rs2;
        $this->assertInstanceOf(RelationStub2::class, $rs2);
        $this->assertEquals('rs2', $rs2->rs2_id);
    }

    public function testBelongsToCreatesModelAsAttributesOnParentWithConversion()
    {
        $m = new RelationStub1(['id' => 'rs1', 'descr' => 'RS 1', 'rs2_id' => 'rs2', 'rs2_descr' => 'RS 2']);
        $rs2 = $m->rs2Converted;
        $this->assertInstanceOf(RelationStub2::class, $rs2);
        $this->assertEquals('rs2', $rs2->id);
    }

    public function testHasManyCreatesModelsInCollection()
    {
        $m = new RelationStub2(['id' => 'rs2', 'descr' => 'RS 2', 'rel1s' => [
            ['id' => 'rs1-1', 'descr' => 'RS 1-1'],
            ['id' => 'rs1-2', 'descr' => 'RS 2-2'],
        ]]);
        $rs1s = $m->rs1s;
        $this->assertInstanceOf(\Illuminate\Support\Collection::class, $rs1s);
        $this->assertEquals('rs1-1', $rs1s->first()->id);
        $this->assertEquals('rs1-2', $rs1s->last()->id);
    }
}

class RelationStub1 extends VO
{

    use FakeRelations;

    public function rs2()
    {
        return $this->fakeBelongsTo(RelationStub2::class);
    }

    public function rs2Converted()
    {
        return $this->fakeBelongsTo(RelationStub2::class, ['id' => 'rs2_id', 'descr' => 'rs2_descr']);
    }
}

class RelationStub2 extends VO
{

    use FakeRelations;

    public function rs1s()
    {
        return $this->fakeHasMany(RelationStub1::class, [], 'rel1s');
    }
}
