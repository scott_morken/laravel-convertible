<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/2/16
 * Time: 7:55 AM
 */
use Mockery as m;

class ModelTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown()
    {
        m::close();
    }

    /**
     * @expectedException \Smorken\Convertible\Models\ReadOnlyException
     */
    public function testReadOnlyIsDefault()
    {
        $m = new ModelStub();
        $m->save(['foo' => 'bar']);
    }

    /**
     * @expectedException \Smorken\Convertible\Models\ReadOnlyException
     */
    public function testReadOnlyOnUpdate()
    {
        $m = new ModelStub();
        $m->exists = true;
        $m->unguard(true);
        $m->update(['foo' => 'bar']);
    }

    /**
     * @expectedException \Smorken\Convertible\Models\ReadOnlyException
     */
    public function testReadOnlyOnCreateOrUpdate()
    {
        $m = new ModelStub();
        $m->createOrUpdate(['foo' => 'bar'], ['fiz' => 'buz']);
    }

    /**
     * @expectedException \Smorken\Convertible\Models\ReadOnlyException
     */
    public function testReadOnlyIsSetByDisallowWrite()
    {
        $m = new ModelStub();
        $m::allowWrite();
        $m::disallowWrite();
        $m->save(['foo' => 'bar']);
    }

    public function testGetColumns()
    {
        $m = new ModelStub();
        $cols = $m->getColumns();
        $expected = [
            'foo_col',
            'bar_col',
        ];
        $this->assertEquals($expected, $cols);
    }

    public function testGetColumnsWithAlias()
    {
        $m = new ModelStub();
        $cols = $m->getColumns(false, 'a');
        $expected = [
            'a.foo_col',
            'a.bar_col',
        ];
        $this->assertEquals($expected, $cols);
    }

    public function testGetColumnsQualified()
    {
        $m = new ModelStub();
        $cols = $m->getColumns(true);
        $expected = [
            'model_stubs.foo_col',
            'model_stubs.bar_col',
        ];
        $this->assertEquals($expected, $cols);
    }

    public function testGetColumnsQualifiedWithAlias()
    {
        $m = new ModelStub();
        $cols = $m->getColumns(true, 'a');
        $expected = [
            'a.foo_col',
            'a.bar_col',
        ];
        $this->assertEquals($expected, $cols);
    }

    public function testCarbonDateConversion()
    {
        $m = new ModelStub(['test_date' => '2000-01-01']);
        $this->assertInstanceOf('Carbon\Carbon', $m->test_date);
    }

    public function testCarbonDateConversionCanBeNull()
    {
        $m = new ModelStub();
        $this->assertNull($m->test_date);
    }

    public function testGetRelationClass()
    {
        $m = new ModelStub();
        $this->assertEquals(FooStub::class, $m->testRelationClass());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testGetRelationClassThrowsException()
    {
        $m = new ModelStub();
        $m->testInvalidRelationClass();
    }

    public function testAllowWriteOverridesReadOnly()
    {
        $res = m::mock('Illuminate\Database\ConnectionResolverInterface');
        $conn = m::mock('Illuminate\Database\ConnectionInterface');
        $grammar = m::mock('Illuminate\Database\Query\Grammars\Grammar');
        $proc = m::mock('Illuminate\Database\Query\Processors\Processor');
        $res->shouldReceive('connection')->andReturn($conn);
        $conn->shouldReceive('getQueryGrammar')->andReturn($grammar);
        $conn->shouldReceive('getPostProcessor')->andReturn($proc);
        $grammar->shouldReceive('getDateFormat')->andReturn('Y-m-d');
        $grammar->shouldReceive('compileInsertGetId')->andReturn('id');
        $proc->shouldReceive('processInsertGetId')->andReturn('id');
        $m = new ModelStub();
        $m::setConnectionResolver($res);
        $m::allowWrite();
        $this->assertTrue($m->save(['foo' => 'bar']));
    }

    public function testScopeDefaultColumns()
    {
        $m = new ModelStub();
        $q = m::mock('Query');
        $args = [
            'model_stubs.foo_col',
            'model_stubs.bar_col',
        ];
        $q->shouldReceive('select')->once()->with($args);
        $this->assertEquals($q, $m->scopeDefaultColumns($q));
    }

    public function testScopeDefaultColumnsAliased()
    {
        $m = new ModelStub();
        $q = m::mock('Query');
        $args = [
            'a.foo_col',
            'a.bar_col',
        ];
        $q->shouldReceive('select')->once()->with($args);
        $this->assertEquals($q, $m->scopeDefaultColumns($q, 'a'));
    }

    public function testSimpleWhereWithValue()
    {
        $m = new ModelStub();
        $q = m::mock('Query');
        $q->shouldReceive('where')->once()->with('foo', '=', 'bar');
        $this->assertEquals($q, $m->scopeWhereTest($q));
    }

    public function testSimpleWhereWithNoValueSkipsWhere()
    {
        $m = new ModelStub();
        $q = m::mock('Query');
        $q->shouldReceive('where')->never();
        $this->assertEquals($q, $m->scopeWhereTest($q, null));
    }

    public function testTermLike()
    {
        $m = new ModelStub();
        $q = m::mock('Query');
        $q->shouldReceive('where')->once()->with('model_stubs.foo_col', 'LIKE', '_123');
        $this->assertEquals($q, $m->scopeTermLike($q));
    }

    public function testTermLikeNotThreeChars()
    {
        $m = new ModelStub();
        $q = m::mock('Query');
        $q->shouldReceive('where')->once()->with('model_stubs.foo_col', '=', '1234');
        $this->assertEquals($q, $m->scopeTermLike($q, '1234'));
    }

    public function testRelationMapGetterSetter()
    {
        $m = new ModelStub();
        $m->setRelationMap(['foo' => 'bar']);
        $this->assertEquals(['foo' => 'bar'], $m->getRelationMap());
    }
}

class DB
{
    public function raw($string)
    {
        return $string;
    }
}

class ModelStub extends \Smorken\Convertible\Models\Eloquent\BaseModel
{

    protected $columns = [
        'foo_col',
        'bar_col',
    ];

    protected $relation_map = [
        'foos'   => FooStub::class,
    ];

    public function testInvalidRelationClass()
    {
        return $this->getRelationClass('bars');
    }

    public function testRelationClass()
    {
        return $this->getRelationClass('foos');
    }

    public function getTestDateAttribute()
    {
        return $this->toCarbonDate('test_date');
    }

    public function scopeWhereTest($query, $value = 'bar')
    {
        return $this->simpleWhere($query, 'foo', $value);
    }

    public function scopeTermLike($query, $term_id = '123', $alias = null)
    {
        return $this->whereTermLike('foo_col', $query, $term_id, $alias);
    }
}

class FooStub extends \Smorken\Convertible\Models\Eloquent\BaseModel
{

}
