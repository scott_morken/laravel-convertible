<?php
use Smorken\Convertible\Models\VO;

class VOTest extends \PHPUnit\Framework\TestCase
{

    public function testGetAttributeMagicMethod()
    {
        $m = new VOStub(['fiz' => 'buz']);
        $this->assertEquals('buz', $m->fiz);
    }

    public function testGetAttributeWithNoMatchIsNull()
    {
        $m = new VOStub();
        $this->assertNull($m->getAttribute('fiz'));
    }

    public function testGetAttributeWithGetMethod()
    {
        $m = new VOStub();
        $this->assertEquals('bar_test', $m->bar);
    }

    public function testSetAttributeMagicMethod()
    {
        $m = new VOStub();
        $m->fiz = 'buz';
        $this->assertEquals('buz', $m->fiz);
    }

    public function testSetAttributeWithSetMethod()
    {
        $m = new VOStub();
        $m->foo = 'buz';
        $this->assertEquals('buztest', $m->foo);
    }

    public function testArrayGet()
    {
        $m = new VOStub();
        $m->fiz = 'buz';
        $this->assertEquals('buz', $m['fiz']);
    }

    public function testArraySet()
    {
        $m = new VOStub();
        $m['fiz'] = 'buz';
        $this->assertEquals('buz', $m->fiz);
    }

    public function testArrayIsset()
    {
        $m = new VOStub();
        $m['fiz'] = 'buz';
        $this->assertTrue(isset($m['fiz']));
    }

    public function testArrayUnset()
    {
        $m = new VOStub();
        $m['fiz'] = 'buz';
        unset($m['fiz']);
        $this->assertNull($m['fiz']);
    }

    public function testCasts()
    {
        $m = new VOStub();
        $m['jsonfiz'] = ['foo' => 'bar'];
        $attrs = $m->getAttributes();
        $this->assertEquals('{"foo":"bar"}', $attrs['jsonfiz']);
        $this->assertEquals(['foo' => 'bar'], $m->jsonfiz);
    }

    public function testDateFromString()
    {
        $m = new VOStub();
        $m->foodate = '2001-01-01';
        $this->assertInstanceOf(\Carbon\Carbon::class, $m->foodate);
    }

    public function testDateFromDateTime()
    {
        $m = new VOStub();
        $m->foodate = new DateTime('now');
        $this->assertInstanceOf(\Carbon\Carbon::class, $m->foodate);
    }

    public function testHiddenHides()
    {
        $m = new VOStub();
        $m->fuz = 'secret';
        $m->bar = 'buz';
        $m->biz = 'secret 2';
        $m->addHidden(['biz']);
        $this->assertEquals(['bar' => 'buz'], $m->toArray());
    }

    public function testVisibleShows()
    {
        $m = new VOStub2();
        $m->fuz = 'secret';
        $m->bar = 'buz';
        $m->biz = 'baz';
        $m->addVisible('biz');
        $this->assertEquals(['bar' => 'buz', 'biz' => 'baz'], $m->toArray());
    }

    public function testToJson()
    {
        $m = new VOStub();
        $m->bar = 'buz';
        $m->foodate = '2017-06-07 00:00:00';
        $this->assertEquals('{"bar":"buz","foodate":"2017-06-07 00:00:00"}', $m->toJson());
    }
}

class VOStub extends VO
{

    protected $casts = ['jsonfiz' => 'json'];

    protected $dates = ['foodate'];

    protected $hidden = ['fuz'];

    public function setFoo($v)
    {
        $this->attributes['foo'] = $v . 'test';
    }

    public function getBar()
    {
        return 'bar_test';
    }
}

class VOStub2 extends VO
{
    protected $visible = ['bar'];
}
