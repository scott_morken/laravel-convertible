<?php

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 1/29/16
 * Time: 8:05 AM
 */
class ConvertibleTest extends \PHPUnit\Framework\TestCase
{

    public function testInstantiate()
    {
        $sut = new ConvertibleStub();
        $this->assertInstanceOf('Smorken\Convertible\Contracts\Convertible', $sut);
    }

    public function testSimpleConversion()
    {
        $sut = new ConvertibleStub(['foo' => 'test']);
        $this->assertEquals('test', $sut->bar);
    }

    public function testSimpleConversionNotConvertibleReturnsNormal()
    {
        $sut = new ConvertibleStub(['foo' => 'test', 'fizz' => 'test2']);
        $this->assertEquals('test2', $sut->fizz);
    }

    public function testClosureConversion()
    {
        $sut = new ConvertibleStub(['foo' => 'test', 'fizz' => 'test2']);
        $r = $sut->baz;
        $this->assertTrue((int)$r > 0 && (int)$r < 10);
    }

    public function testClosureConversionWithAttributeName()
    {
        $sut = new ConvertibleStub(['foo' => 'test', 'fizz' => 'test2', 'IDCOL' => '3']);
        $this->assertEquals('3', $sut->id);
    }

    public function testToVoInstanceOfContract()
    {
        $sut = new ConvertibleStub(['foo' => 'test', 'fizz' => 'test2']);
        $vo = $sut->toVo();
        $this->assertInstanceOf('Smorken\Convertible\Contracts\VO', $vo);
    }

    public function testToVoHasAttributes()
    {
        $sut = new ConvertibleStub(['foo' => 'test', 'fizz' => 'test2']);
        $vo = $sut->toVo();
        $this->assertEquals('test', $vo->bar);
    }

    public function testHasAttributesCaseInsensitive()
    {
        $sut = new ConvertibleStub(['foo' => 'test', 'FUZ' => 'test3']);
        $this->assertEquals('test3', $sut->buz);
    }

    public function testOverrideConversionCreatesVo()
    {
        $sut = new ConvertibleStub(['foo' => 'test', 'fizz' => 'test2']);
        $vo = $sut->toVo($sut->getConversion('override'));
        $this->assertNull($vo->bar);
        $this->assertEquals('test', $vo->biz);
    }
}

class ConvertibleStub extends \Smorken\Convertible\Models\Eloquent\BaseModel
{

    use \Smorken\Convertible\Models\Traits\Convert;

    protected $fillable = [
        'IDCOL',
        'foo',
        'fizz',
        'FUZ'
    ];

    public function name()
    {
        return 'foo';
    }

    public function conversions()
    {
        return [
            'default'  => [
                'id' => function ($attr, $self) {
                    return array_get($this->attributes, 'IDCOL');
                },
                'bar' => 'foo',
                'baz' => function ($attr, $self) {
                    return rand(1, 9);
                },
                'buz' => 'FUZ',
            ],
            'override' => [
                'biz' => 'foo',
            ],
        ];
    }
}
